######

These scripts where written with the scope of both learning regular expressions and familiarizing myself with the Ruby language. 

They each process and alter text files based on pattern-matching and substitution tasks.

Written in Spring 2015 as an assignment for my Programming Languages course.  

######
